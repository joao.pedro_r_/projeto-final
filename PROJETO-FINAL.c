#define _GNU_SOURCE
#include <unistd.h>
#include <stdlib.h>
#include <malloc.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <sched.h>
#include <stdio.h>
#include <pthread.h>


#define FIBER_STACK 1024*64
#define NUM_THREADS     100

struct c {
        int saldo;
};

typedef struct c conta;

conta conta1, conta2;
int valor = 1;


void* transferencia1 (void *threadid)
{
	long rep;
	rep = (long)threadid;
                if (conta1.saldo >= valor){
                        conta1.saldo -= valor;
                        conta2.saldo += valor;
					}
		else {
			exit (1);
		}
         printf ( "\nTranferencia concluida com sucesso!\n");
         printf ( "Saldo da conta 1: %d\n", conta1.saldo);
         printf ( "Saldo da conta 2: %d\n ", conta2.saldo);

	 pthread_exit(NULL);
}



void* transferencia2(void *threadid)
{
	long rep2;
	rep2 = (long)threadid;
                if (conta2.saldo >= valor){
                        conta2.saldo -= valor;
                        conta1.saldo += valor;
                			}
		else{
			exit (1);
		}
         printf ( "\nTranferencia concluida com sucesso!\n");
         printf ( "Saldo da conta 1: %d\n", conta1.saldo);
         printf ( "Saldo da conta 2: %d\n ", conta2.saldo);
	 pthread_exit(NULL);
}

void main (){
        void* stack;
        stack= malloc (FIBER_STACK);
        if (stack == 0){
                perror ("malloc: could not allocate stack");
                exit (1);
        }
	int v1;
	long t;
	int escolha;
	printf("Insira a conta desejada: \n[1] para conta 1.\n[2] para conta 2.\n");
	scanf ("%d", &escolha);

        conta1.saldo = 100;
        conta2.saldo = 100;
	printf ("\nTranferindo para a conta o valor de : R$ %d\n", valor);



        pthread_t t1[NUM_THREADS];
	if (escolha == 1){
		for(t=0; t<NUM_THREADS; t++){
			v1 = pthread_create(&t1[t], NULL, transferencia1, (void *)t);
			if (v1){
         			printf("ERROR");
         			exit(-1);
			}
		}
	}
	else if (escolha == 2){
		for(t=0; t<NUM_THREADS; t++){
			v1 = pthread_create(&t1[t], NULL, transferencia2, (void *)t);
			if (v1){
                                printf("ERROR");
                                exit(-1);
                        }
		}
	}

        free (stack);
        printf ("Tranferencias concluidas e memoria liberada\n");

}
