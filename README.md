# Projeto-final

#Para compilar em Linux: gcc PROJETO-FINAL.c -o projeto.bin -lpthread

#Para compilar em Unix: deve-se instalar o comando homebrew. E compilar da seginte forma: cc PROJETO-FINAL.c -o projeto.bin -lpthread 

#para executar: ./projeto.bin

#O objetivo do código é zerar uma das contas escolhida pelo usuário, então para comprovar se os resultados propostos foram alcançados uma das contas no final da execução do programa deve está com o saldo igual a 0.
